# Données sur les profils d'acheteurs

Ces données ont vocation à recenser les acheteurs publics français et leurs « profils », c'est à dire les sites Internet où l’État, les collectivités territoriales ainsi que l'ensemble des établissements soumis aux règles de passation des marchés publics rendent accessibles les documents de marchés publics ainsi que les données essentielles de la commande publique (DECP).

## Profils d'acheteur

L'article [R. 2132-3](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000037730733) du code de la commande publique dispose que « le profil d’acheteur est la plateforme de dématérialisation permettant notamment aux acheteurs/autorités concédantes de mettre les documents de la consultation à disposition des opérateurs économiques par voie électronique et de réceptionner par voie électronique les documents transmis par les candidats et les soumissionnaires ».

Liste de profils d'acheteurs ➡ [profils_acheteurs.csv](/profils_acheteurs.csv)

| siret_acheteur | nom_acheteur | url_profil_acheteur | nom_service_profil_acheteur |
|----------------|--------------|---------------------|-----------------------------|

## Fournisseurs de profils d'acheteur

Les fournisseurs de profils d'acheteur sont les entreprises qui fournissent aux acheteurs les sites Internet, technologies et autres moyens techniques leur permettant de publier leur profil d'acheteur.

Liste de fournisseurs de profils d'acheteurs ➡ [fournisseurs_profils_acheteurs.csv](/fournisseurs_profils_acheteurs.csv)

| nom_service_profil_acheteur | url_service_profil_acheteur | siret_fournisseur_service_profil_acheteur | nom_fournisseur_service_profil_acheteur | url_fournisseur_service_profil_acheteur |
|-----------------------------|-----------------------------|-------------------------------------------|-----------------------------------------|-----------------------------------------|
